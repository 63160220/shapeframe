/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


/**
 *
 * @author os
 */
public class RectangleFrame extends JFrame {
    JLabel lblHeight;
    JLabel lblWidth;
    JTextField txtHeight;
    JTextField txtWidth;
    JButton btnClaculate;
    JLabel lblResult;
    public RectangleFrame() {
        super("Rectangle");
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblHeight = new JLabel("height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 5);
        this.add(txtHeight);
        
        lblWidth = new JLabel("width:", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(100, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);
        
        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(155, 5);
        this.add(txtWidth);
        
        btnClaculate = new JButton("Claculate");
        btnClaculate.setSize(100, 20);
        btnClaculate.setLocation(220, 5);
        this.add(btnClaculate);
        
        lblResult = new JLabel("Rectangle height = ??? width = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnClaculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strheight = txtHeight.getText();
                double height = Double.parseDouble(strheight);
                String strwidth = txtWidth.getText();
                double width = Double.parseDouble(strwidth);
                Rectangle rectangle = new Rectangle(height,width);
                lblResult.setText("rectangle height = "+String.format("%.2f",rectangle.getHeight())
                        +" width = "+String.format("%.2f",rectangle.getWidth())
                        +" area = " + String.format("%.2f",rectangle.calArea()) 
                        + " perimeter = " + String.format("%.2f",rectangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }    
        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}


