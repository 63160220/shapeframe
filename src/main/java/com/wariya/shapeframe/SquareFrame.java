/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author os
 */
public class SquareFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Square");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.white);
        lblSide.setOpaque(true);
        frame.add(lblSide);

        final JTextField txtSide= new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        frame.add(txtSide);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Circle radius = ? area = ? perimeter = ? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class    
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    //1.ดึงข้อมูล text  จาก  txtSide -> strSide
                    String strSide = txtSide.getText();
                    //2.แปลง strSide -> side: double parseDouble
                    double side  = Double.parseDouble(strSide);
                    //3.instance object Square(side) -> square
                    Square square = new Square(side);
                    //4.update lblResult โดยนำข้อมูลจาก Square ไปแสดงให้ครบถ้วน
                    lblResult.setText("square s = " + String.format("%.2f", square.getSide())
                            + " area = " + String.format("%.2f", square.calArea())
                            +"  perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText(" ");
                    txtSide.requestFocus();
                }
            }
        });
        
        frame.setVisible(true);
    }
}

