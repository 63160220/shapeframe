/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.shapeframe;

/**
 *
 * @author os
 */
public class Triangle extends Shape{
    private double height;
    private double base;
    private double x;
    
    public Triangle(double height,double base) {
        super("Triangle");
        this.height=height;
        this.base=base;
        this.x=(Math.sqrt((Math.pow(height, 2))+(Math.pow(base, 2))));
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    @Override
    public double calArea() {
        return 0.5*height*base;
    }

    @Override
    public double calPerimeter() {
        return x+height+base;
    }
    
}

