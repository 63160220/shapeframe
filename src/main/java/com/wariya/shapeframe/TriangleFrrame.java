/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author os
 */
public class TriangleFrrame extends JFrame{
    JLabel lblHeight;
    JLabel lblBase;
    JTextField txtHeight;
    JTextField txtBase;
    JButton btnClaculate;
    JLabel lblResult;
    public TriangleFrrame() {
        super("Triangle");
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblHeight = new JLabel("height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 5);
        this.add(txtHeight);
        
        lblBase = new JLabel("base:", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(100, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(155, 5);
        this.add(txtBase);
        
        btnClaculate = new JButton("Claculate");
        btnClaculate.setSize(100, 20);
        btnClaculate.setLocation(220, 5);
        this.add(btnClaculate);
        
        lblResult = new JLabel("Triangle height = ??? base = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnClaculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strheight = txtHeight.getText();
                double height = Double.parseDouble(strheight);
                String strwidth = txtBase.getText();
                double base = Double.parseDouble(strwidth);
                Triangle triangle = new Triangle(height,base);
                lblResult.setText("Triangle height = "+String.format("%.2f",triangle.getHeight())
                        +" base = "+String.format("%.2f",triangle.getBase())
                        +" area = " + String.format("%.2f",triangle.calArea()) 
                        + " perimeter = " + String.format("%.2f",triangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(TriangleFrrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }    
        });
    }

    public static void main(String[] args) {
        TriangleFrrame frame = new TriangleFrrame();
        frame.setVisible(true);
    }
}

