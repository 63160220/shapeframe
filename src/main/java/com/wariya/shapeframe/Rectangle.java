/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.shapeframe;

/**
 *
 * @author os
 */
public class Rectangle extends Shape{
    private double height;
    private double width;

    public Rectangle(double height,double width) {
        super("Rectangle");
        this.height=height;
        this.width=width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
    
    @Override
    public double calArea() {
        return height*width;
    }

    @Override
    public double calPerimeter() {
       return (2*height)+(2*width);
    }
    
}

